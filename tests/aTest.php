<?php

require "../vendor/autoload.php";
require "../src/addOne.php";

use PHPUnit\Framework\TestCase;

class aTest extends TestCase
{
    public function testAddOne()
    {
        $testArrays = array(
            [1],
            [9],
            [1, 2, 3],
            [9, 9, 9],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [0, 0, 0, 2],
            [0, 0, 0, 2, 0, 0, 0, 0],
            [1, 9, 2]
        );

        $checkArrays = array(
            [2],
            [1, 0],
            [1, 2, 4],
            [1, 0, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 2],
            [0, 0, 0, 3],
            [0, 0, 0, 2, 0, 0, 0, 1],
            [1, 9, 3]
        );

        //addOneMakeToNumber
        $this->assertEquals(addOneMakeToNumber($testArrays[0]), $checkArrays[0]);
        $this->assertEquals(addOneMakeToNumber($testArrays[1]), $checkArrays[1]);
        $this->assertEquals(addOneMakeToNumber($testArrays[2]), $checkArrays[2]);
        $this->assertEquals(addOneMakeToNumber($testArrays[3]), $checkArrays[3]);
        $this->assertEquals(addOneMakeToNumber($testArrays[4]), $checkArrays[4]);
        $this->assertNotEquals(addOneMakeToNumber($testArrays[5]), $checkArrays[5]); //attention
        $this->assertNotEquals(addOneMakeToNumber($testArrays[6]), $checkArrays[6]); //attention
        $this->assertEquals(addOneMakeToNumber($testArrays[7]), $checkArrays[7]);

        //addOneArrayCheck
        $this->assertEquals(addOneArrayCheck($testArrays[0]), $checkArrays[0]);
        $this->assertEquals(addOneArrayCheck($testArrays[1]), $checkArrays[1]);
        $this->assertEquals(addOneArrayCheck($testArrays[2]), $checkArrays[2]);
        $this->assertEquals(addOneArrayCheck($testArrays[3]), $checkArrays[3]);
        $this->assertEquals(addOneArrayCheck($testArrays[4]), $checkArrays[4]);
        $this->assertEquals(addOneArrayCheck($testArrays[5]), $checkArrays[5]);
        $this->assertEquals(addOneArrayCheck($testArrays[6]), $checkArrays[6]);
        $this->assertEquals(addOneMakeToNumber($testArrays[7]), $checkArrays[7]);
    }
}
